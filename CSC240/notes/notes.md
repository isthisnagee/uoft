<style>
@font-face {
  font-family: 'cm';
  src:  url('../../cmunrm.ttf') format('ttf');
}

* {
font-family: 'cm';
}
</style>

<h1>CSC240: An Enriched Introduction to the Theory of Computation</h1>
<h2>Table of Contents</h2>

[TOC]

##Logic

* A **predicate** is a proposition whose truth depends on 1+ variables.
For example, 
$$ x^3 \geq 8 \text{ is true only when }x \geq 2$$

Let's look at this database:

| Employee      | Gender        | Salary |
| ------------- |:-------------:| -----: |
| andy          |       M       | 0      |
| donna         |       F       | 3000   |
| Leslie        |       F        | 5000   |
| Ron           |       M       | 5700   |
| Tom           |       M       | 1800   |

Let $E$ denote the set of empolyees, and 
let  $a: E \rightarrow \{T,F\}$ where
$a(e) \mapsto T$ if the empolyee $e$ made  1000, and $F$ otherwise,
be a function. Let $s(e)$ be "salary of employee $e$".

**A *predicate* is a function whose range is $\{T,F\}$**, thus $a$ is a predicate, but $s$ is not (alternatively, we can say $s(e)$ is not a predicate, but $s(e) \geq 1000$ is).

Let $a(e)$ mean that "employee $e$ made at least 1000", and $E$ be the
set of employees.

$$ \forall e \in E. a(e) $$

That is a false statement, since if we let $e = $ andy, then $a($ andy$)$ 
would say "the employee andy made at leat 1000", but andy made 0. Thus we have provided a counter example.

Let $l(e)$ mean that "the employee $e$ made less than 6000".
$$ \forall e \in E. l(e)$$

This is a true statement, we can verify this by plugging in every 
employee and verifying. Alternatively, we could use $s(e)$ from above, 
and write $\forall e \in E. s(e) < 6000$.

Lets consider the following statement:

* Every employee made at least 1000 or every employee made less than 5000.

Is the statement true? Though it is not as useful for this example, writing out the statement logically is usually helpful, it condenses the statement and removes ambiguity. 

$$ (\forall e \in E. s(e) \geq 1000)  \text{ OR } (\forall e \in E. s(e) < 5000) $$

This statement is false since choosing Andy and Ron provide a counter example.

Notice that  $\forall e \in E. (s(e) \geq 1000)  \text{ OR } (s(e) < 5000) $ is true, and is not equivalent to the above. With that in mind, it would be better to write 
$$ (\forall e \in E. s(e) \geq 1000)  \text{ OR } (\forall m \in E. s(m) < 5000) $$

This way we specify that the two choices can be differnt.

###Validity

####Propositional Formula
A **propositional formula** is an expression build up from Boolean variables using connectinves (AND, OR, NOT, etc), and does not contain predicates or quantifiers.

They are **valid** (or a **tautology**) if all of the truth table entries are true. For example `P OR NOT(P)` is always true (whenever P is true, the statement is true, and if P is false, NOT(P) is true).

They are **unsatisfiable** or a **contradiction** if all the entries in the truth table are false. For example, `P AND NOT(P)` is a contradiction.

####Truth Assignment
A **truth assignment** is a function from a set of propositional values to $\{T,F\}$. For example $T:\{P, Q\} \rightarrow \{T, F\}$.

###SAT
This is the **satisfiability problem**, which is used to decide whether a given propositional formula is satisfiable. SAT takes in a propositional formual, and gives YES if it is satisfiable or NO if it is not. 

We do not know if SAT is in P, however, SAT $\in $ NP.

P = all decision problems that can be solved in polynomial time.

NP = all decision problems that can be verified in polynomail time.

SAT is in P if and only if P = NP. This is the Cook theorem.

###DNF
A **literal** is a variable or the negation of a variable. SAT is easy to solve for a conjuction of literals.

A propositional formula is in *disjuntive normal form* if it is a disjunction of conjuctions of literals.


<u>Theorem</u>

Every propositional formula is logically equivalent to a propositional formula in DNF.


###CNF

A propositional formula is in **conjunctive normal form** if it is a conjunction of disjunctions of literals.

A **clause** is a disjunction of literals, so a CNF formula is a conjunction of clauses

<u>Theorem</u>

Every propositional formula is logically equivalent to a propositional formula in CNF.

###CNF-SAT

Decide whether a given propositional formula in CNF is satisfiable. 

Its input is a propositional formula in CNF, and its output is YES if it is satisfiable and NO if it is unsatisfiable. 

CNF-SAT is just as hard as SAT :(

### Predicate Logic Formula

An expression built up from predicate symbols, each having a fixued number of arguments, using connectives and existential/universal quantifiers.

The arguments of the predicate symbols are variables and constans from specific domains.

$x$ is quantified if it occurs within a $\forall$ or $\exists$. Otherwise it's called a **free variable**

A **valuation** maps each free variable to a domain element.

$E$ and $E'$ are **equivalent** if $ E \iff E'$

###Prenex Normal Form

A predicate logic formula is in this prenex normal form if and only if 
it is in the form

$$ Q_1x_1 \in D_1, Q_2 x_2 \in D_2, \dots, Q_kx_k \in D_k, E(x_1, \dots, x_k) $$

Where $E(x_1, \dots, x_k)$ is a formula without quantifiers, and for $i = 1,\dots,k$, $Q_i$is either $\forall$ or $\exists$.

 

