<style>
@font-face {
  font-family: 'cm';
  src:  url('../../cmunrm.ttf') format('ttf');
}

* {
font-family: 'cm';
}
</style>

<h1>CSC258: Computer Organization</h1>
<h2>Table of Contents</h2>

[TOC]

<!--## is a header, ### is a subheader, ... -->

##Introduction
    
This is an introduction to the underlying structures of computers, at the very fundemental level. We will look at the components that make everything work.

###Goals
* understand underlying architecture of computer systems
* learn how to use architecture to store data and create behavior
* create digital logic solutions to given problems

###Integers

How are integers stored? Say you have the number 1234, in the computer, it
is represented as ones and zeroes in the computer. The values that an integer can have is limited.

* *Signed Integer*: range from $ -2^{31}$ to $ 2^{31} -1 $
* *Unsigned Integer*: range from $0$ to $2^{32} -1$
* there are different ranges for long (32 bits), short (16 bits), and byte (8 bits).


#Logic

###Gates

* AND gates. takes 2 inputs.
* OR gates. takes 2 inputs.
* NOT gate. takes one input and switches.
* XOR. takes 2 inputs. only tru if $A$ and $B$ are different.
* NAND gate. Is the opposite of AND gates. Any logical circuit can be built
  with a NAND gate. (take this on a deserted island)
* NOR gate. Is the same as OR + NOT
* BUFFER.

Creating complex circuits is the same as working w/ logic in any programming language.

Say we have 
```
Y = (!A and !B and !C) or (!A and B and C) or (A and B and C)
```

We can draw that with gates as follows:
<!--TODO-->

####True and False

True and false refer to electrical voltage on the wires

* Logical Zero: little to no voltage at that point
* Logial 1: typicallly a volgtage difference of 5 volts, relative to the ground.

Logic gates behave like swithces that control wheter or not the output 
is high volate or low volates

* Switches are physical devices for manualy controlling a circuit
* Transistors are semi-conductors that control a circuit electrically.

##Transistors

Assuming that certian signals can be turned on (1) or off (0), we need
ways to combine these signals together.

So for example, say we press `CTRL` + `ALT` + `DEL` and we want to restart if all are pressed. We only want a 1 if all three are pressed, and we want 0 otherwise, so we need to design a circuit that does that. 

Digital Logic can also be expressed in terms of wave, which include *time*.


They form the basics of all computer hardware, and are used (in the scope of this course) for digital logic design.

####What do they do

Essentially, they connect $A$ to $B$ based on the value at $C$.

* If the value at $C$ is high, $A$ and $B$ are connected
    ` A--o--o--B`
* If the value at $C$ is low, $A$ and $B$ are not connected
    ` A--o  o--B`
    
Recall that electricity is teh flow of particles, and electrons want to move from high electrical potential to low electrical potential (the way water flows from the resevoir to the ground). The potential is referred to as *voltage* (elevation of water above ground), and the rate of the flow is the *current* (how fast the water travels).

Resistance is measured in Ohms ($\Omega$). Resistance ($I$) is $\frac{V}{R}$, where $V$ is voltage, and $R$ is current.

Where do these electrons come from? First, from batteries, these eventually run out. Most come from outlets, that are constantly being supplied, that ideally never run out.
Ik
Even though electrical current is the flow of electrons through a medium, its direction is opposite the electron flow.

####Doping

p-type is Silicon doped withi phsophorous, n-type is silicon doped with boron
So we get

```
n-type     (-)(-)(-)
------  => ----------
p-type     (+)(+)(+)
```

Or we can look at it as follows

```
o -> o -> o-> (-)|(+)<- 0 <- 0 <- 0
o -> o -> o-> (-)|(+)<- 0 <- 0 <- 0
o -> o -> o-> (-)|(+)<- 0 <- 0 <- 0
                 ^
depletion layer  |
```

Once the depletion layer is wide enough, the doping atoms that remain will create an *electric field*.

Essentially, the electrons move from the n-type to the p-type, but an equilibruim is eventually reached so that
the depletion layer does not grow infinitly.

#####Forward Bias

When a positive voltage is applied to the $p$ end of the juntion above, electrons are injected into the $n$ type. section
This narrrows the depletion layer and increases the electron diffusion rate.

A smaller depletion layer results in electrons traveling easier to the p-type sectoin

#####Reverse Bias

Now a positive voltage is applied to the $n$ end of the junction, so the depletion becomes larger, making it 
harder for electrosn to flow.

Reverse and Forward bias are the basis of transistors, they make "on" and "off" switch.

###Questions

1. T/F: Doping gives a semiconductor an overall positive or negative charge.
    1. False
2. What kind of bias on a pn junction causes a reduction of the depletion layer?
    1. Forward Bias, see 
3. Phosphorous has 5 electrons in its outer valence shell. When added in small amounts to silicon, the result is a __________ semiconductor.
    1. n-type 


##Circuit Creation

Logic gates are good on their own, but you can combine them to build more interesting ciruits.


A circuit is essentially a box with $n$ outputs and $m$ inputs.

Say we have $A,B,C$ inputs, and $X,Y$ outputs. What logic is needed for $X$ high when all three inputs are high? You'd use $A$ AND $B$ AND $C$.

What about $Y$ high when the number of high inputs is odd?
You'd use $(A$ XOR $B)$ XOR $C$

So small problems can be solved easily, but larger problems need a systematic approach. So how do we approach problems like these?

###Creating complex logic:

1. create truth tables.
2. Express as a boolean expression.
3. Convert to gates.

The key to an efficient design is spending time on part 2


###Minterms and Maxterms

They are an easier way to express 

Row 1 : 0 0 0 => m_0

Row 2 : 0 0 1 => m_1

<u>Definition</u>

1. A **minterm** is an AND expression with every input present in true or completed form.
2. A **maxterm** is an OR expression w/ every input present in true or completed form.


####Sum/Products of Minterms/Maxterms (SOM/POM):

since each minterm corresponds to a single output, the combined outputs are a union of these minterm expressions for a **sum of minterms**

The  **sum of maxterms** is the intersection of the maxterms.


Converting SOMS to gates is really easy. Say you have the following, assuming 3 inputs
$$ m_0 + m_1 + m_2 + m_3 = (\neg A\land \neg B \land \neg C) \lor (\dots) \lor (\dots )$$


Let's look at a 2-input XOR gate. Keep in mind $m_x = M_x'$ 

* SOM: $F = m_1 + m_2$
* POM: $F = M_0 \cdot M_3$
* $F'$ in SOM: $(F')' = (m_0 + m_3)' = M_0 \cdot M_3 = F$


### Karnaugh Maps

These are also caled K-maps. These are 2-d grids of minterms, where adjacent minterm locatins in the grid differ by a single literal, and the values are the output for that minterm

```
__ |nBnC nBC BC Bnc
nA | 0   0   1  0
A  | 1   0   1  1 
```


