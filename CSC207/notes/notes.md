<h1>CSC207 - An Introduction to Software Design</h1>

[TOC]


## Introdution

General course outline, which can be viewd [here](http://www.cdf.toronto.edu/~csc207h/fall/)

Help Centre: Monday-Thursday, 4-6 @ BA2230

__[Java Docs](http://docs.oracle.com/javase/8/docs/api/)__


## Version Control


Two flavors of version control:

 * centralized - a master version exists (svn, csv), which we will use.
     * Never directly modify the master copy. People pull the master, do changes to their version, then commit to the master.
     * almost like a linked list. <br> V1 -> V2 -> V3 -> V4 

 * distributed - (git, mercurial)

 In subversion (svn)
 
 ```
 $ svn co --username g5nagee http://markus.cdf.toronto.edu/svn/csc207-2015-09/g5nagee
 $ svn status
 $ svn init
 $ svn add .
 $ svn commit -m 'message'
 
 ```
 co is checkout.

## Unix and Subversion


Master Repository  -> local copy 1

<p class="push">-> local copy 2 </p>
<p class="push">-> local copy n </p>
<p class="push"><- (this direction is called a commit) </p>
<p class="push">-> (this direction is called an update) </p>

### Unix

* ~ is the home directory.
* ````$ ls```` lists files in a directory
* ````$ ls -F```` puts a slash next to directories
* ````$ cd ```` changes directory.
* ````$ ls -l ```` lists in depth (date created, etc)
* ````$ .. ```` goes up.
* ````$ pwd ```` prints current directory

### SVN
In directory 

c4horton
<p class="pushl"> lab 1</p>
<p class="pushl">svndemo</p>

```
$ svn co https://xyz

A c4horton/svndemo
A c4horton/lab1
Checkoued out revision 23. 
# ^ 23 revisions have been done.

$ cd c4horton
$ ls -F
  lab 1/		svndemo/
$ cd svndemo
$ vim A.txt
Alice
Bob
$ ls -F
  A.txt
$ cat A.txt
Alice 
Bob

...
```

Directory now looks like

c4horton
<p class="pushl"> lab 1</p>
<p class="pushl">svndemo</p>
<p class="pushl"><span class="pushl pushl">A.txt</span><p>

```
...

$ svn status
?		A.txt
$ svn add A.txt
$ svn status
A		A.txt
$ svn commit -m 'hahahah'
Adding A.txt
Transmitting file data .
Committed revision 24.
```
Moving to a different computer

```
Differentmachine $ cd c4horton
Differentmachine $ svn update
A	A.txt
updated to revisoin 24.
Differentmachine $ vim A.txt
Alice
Bob
Charlie
Differentmachine $ svn status
M	A.txt
Differentmachine $ svn commit -m 'updated A.txt'
Sending svndemo/A.txt
Transmitting file data .
Committed revision 25.
```

## Unix and Subversion Tips

* In UNIX, file that begins with `.` is called a dotfile. These are not created by the user
they are created by software to store configuration/preferences. `.` is a way to refer to 
the current direcotry, `..` is a way to refer to the parent directory.

* Use ` $ ls -a ` to show hidden files

* If you move/rename files, do it using subversion so _it_ knows what changed.

* Make sure that whenever you sit down to work, you make sure that the repo is
synced with the master.

* SVN commands can only be used in a repo (duh)

* To rename a file in UNIX, use `mv a b`, where a and b are files command. 
  **DON'T DO THIS IN A REPO** 
  To undo, `mv b a`. This will "mess up" svn repo.
  To rename a file w/o "messing up" the svn, do `svn mv a b`

* To move a file in UNIX, use `mv a f`, where a is a file and f is a file.
  **DONT DO THIS IN A REPO**
  
* To remove a file in UNIX, use `rm a`, where a is a file. If you want
  the file back and you are in a repo, use `svn revert a` where is a file
  
## Java

```java
public class HelloWorld{
    // single line comments
    /*
       multi-line comments
    */
    
    /**
      This is a javadoc comment.
      Similar to python's docstring.
    */
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}

```
* Quotes must use double quotes.
* main(==String[] args) declares type
* to run a .java file ` $ javac a `, where a is a filename, to compile, then `$ java a ` to run it.
* `void` means the function has no return
* `public` let's other classes use the methods, `private` makes...private
* `System` 

## More Java


Every variable has to have a (declared) type.

```java
int x; //declares x
x = 4;
int y = 4; //shorthand for the above 2 lines
x = "string"; //will not compile

```



_Interpreted_ languages translates and executes one line at a time (which allows for a shell),
_compiled_ languages translate the whole program then executes. Java is a hybrid.

Compiled languages allow for automatic optimizations.



## Types & Shortcuts

### Types
* Lower case letters are primitive types, and the values are stored diretly as
  that type. Uppercase letters are classes. Notice how primites store directly,
  classes dont.
  
  ```java
  int age = 5;
  int age [ 5 ] // not memory address, unlike in python
  // ^ is a primitive type, below is
  String s1 = new String("hello");
  String s1 [ 0x5 ] --> [0x5, String, "Hello"] // memory address 
  ``` 
  
* Every primitive type is part of a class type.

	```java
	Integer i2 = new Integer(5) //constructs an Int object
	Integer i2 [ *address*  ] --> [*address*, Integer, 5]
	```

### Shortcuts

```java
// the long way :(
int n;
n = 42;
Integer boxed;
boxed = new Integer(11);
// shortcuts
boxed = 11; //auto-boxing
boxed = n + 3;
n = boxed + n; //unboxes
System.out.println("n is " + n + " and boxed is " + boxed);

//with Strings
String s = new String("hello"); // long way 
String s2 = "Bye";
//Strings are immutable, so when adding/changing strings, it creates a new one.
```
##### Arrays
```java
int[] intArray = new int[4]; // 4 represents the size. |intArray| is always 4
intArray = new int[] {1,2,3};
```
##### Classes
```java
public class Circle {
    private String radius;
}
```
`radius` is an _instance variable_. Now each object/instance of the ```Cirlce``` class has its own `radius` variable.

Eclispe stuff
```
new Java Object -> new Package (university) -> new Class (Person)
```

```java
package university;

public class Person {
    private String[] name; //so we can have first name, last name, etc
    private String dob;
    private String gender;
}

// different file

package university;

public class Demo {
    public static void main(String[] args) {
        Person diane = new Person();
        system.out.println(diane);
    }
}
```

##### Constructors

A constructor has: 
	
* the same name as a class
* no return type (not even void!?!?)

A class can have mutiple, unique constructors. The compiler supplies one if it
is not defined.

```java
package university;

public class Person {
    private String[] name; //so we can have first name, last name, etc
    private String dob;
    private String gender;
    // making a constructor, prev. stuff is old.
    public Person(String[] name, String dob, String gender) {
        this.name = name;
        this.dob = dob;
        this.gender = gender;
    }
}

//different file thing
// the previous version is undefined, since a constructor is now defined.
package university;

public class Demo {
    public static void main(String[] args) {
        String[] name = new String[] {"Nagee", "Dean", "Elghassein"};
        Person diane = new Person(name, "1234", "F");
        system.out.println(diane);
    }
}
```
## Basic OOP in Java

### Defining Methods
* A method must have a return type declared, void if nothing returned
* must specify accessibility, `public` or `private (callable only from class)`
* variables declared are local to the method

### Parameters

* For class types, you pasa a reference
* For primitive types, you pass a value (which python doesn't do!)

### Accessibility

How can client code use a private variable? Isn't making public easier?
It protects the client from making well...stupid mistakes. Allows for better
abstraction by client. The developer can change private stuff and not ruin client experience.

This is called **encapsulation**
   
```java
package university;

public class Person {
    
    . . .
    
    // used to view this.gender
    public String getGender() {
        return this.gender;
    }
    
    // used to change this.gender
    public void setGender(String gender) {
        this.gender = gender;
    }
}

```

### Modifiers

* public - class; package; subclass; world
* protected - class; package; subclass
* default - class; package
* private - class

```java
public Class Person {

    . . .
    
    @Override
    public String toString() {
        String result = "";
        
        for (String n: this.name) {
            result = result + n + " ";
        }
        
        result += this.dob;
        
        if (this.gender.equals("M")) {
            result += ", male";
        } else {
            result += ", female";
        }
        
        return result;
   }
}
```

### Inheritance

All classes form a tree called the inheritance heirarchy, with `Object`
at root, which has no parent.

```
Class A prints apple
   |
Class B prints banana
   |
Class C prints cranberry
```
If `C` is called, `apple banana cranberry` is printed.

```java
package university;

public class Student extends Person {

    private String studentNum;
    
    // generate constructor, getter/setter for studentNum

    public Student(String[] name, String dob, String gender,
            String studentNum){
        super(name, dob, gender);
        this.studentNum = studentNum;
    }
    
    @Override
    public String toString() {
        return super.toString() + ", " + this.studentNumb;
    }
}

```

### Multi-part objects

```java
x = new Student(. . .); 
Student x [ 0x5 ] --> [0x5], [Student, studentNum] + 
                      [Person, name, dob, gender] + [Object]                      
```


## Title

### 'Global' class variables

Say we want a count of students, and we dont want the studentCount of 
each student to be 1, but we want one that continues to update.

in the class

```java
private static int studentCount;

\\when declaring it in java's 'init'

studentCount += 1;
\\ vs this.studentCount += 1;
```

### Private and Access

* To allow, say an inherited class to have access to a parent, 
instead of `private`, use `protected`, which gives access to 
that class and any subclass. 

  EX: `protected String studentNum;`

### Inheritance
* Instance methods go from bottom to top.

  EX: toString of an object, say Student, starts from the bottom, so instead of using 
Object's toString, we use Students.

* With variables, it's different. Say we have two objects, Student and Person, 
  where Person is the parent, and both have a `String motto;` in it.
  say `x` is a Student, `x.motto` will be Student's or Person's, depending 
  on the type of `x`.
  
  EX: say `x` is a Person. `x.motto` prints Person's motto.
  EX: say `y` is a Student (which inherits person), `y.motto`  prints Student's motto.
  
  This can be manipulated. Since Student inherits Person, we can **cast**.
  Casting a method will **_not_** work.
  
  ```java
  System.out.println(((Person) y).motto)
  ```
  Will print Person's motto, not the Student motto. Basically, we started from the 
  bottom, now we're here.

### Re-assigning.


Python let us re-assign variables at any time, even changing between types.
Java will not let you.

```java
public static void demoCasting() {
    Object o;
    Person p;
    Student s;
    
    o = new Student(student's stuff);
    p = new Student(other student's stuff);
    
    p = o; //will *not* work.
    
    // but, we can cast
    p = (Person) o; // this will work 
    p = (Student) o; // this will also work.
    // what can we cast? 
    // We can only assign an instance of its parent or children, or itself.
    p = (Student) new String("Hey!"); // this will not work.
}
```

### UML

* Unified Modeling Language allows us to express the design of a program before
  writing any code.
* It is language independent.
* Class diagram:

```
 ------------------------------------------
|              Person (class name)         |  
 ------------------------------------------
|                methods                   |
|                                          |
|                                          |
 ------------------------------------------         
         
```

## Abstract and Interface

### 2-D Arrays
EX:

```java
int[][] table = new int[3][4]; \\ 3 rows 4 columns
int[][] jagged = new int[3][];
jagged[0] = new int[4];
jagged[1] = new int[3];
...
```


### Abstract Classes

Say we want 2 different classes, one for numbers and one for letter grades
and we want to put them all in one array. We can do that with an Object
array, but then you can put anything in it (except a primitive). We can make a 
parent classs for the two grade subclasses (say, letterGrade and numberGrade)


* we cannot make instances of abstract classes (i.e. we can't instantiate it).


```java
public abstract class Grade {
    // we can't initialize* anything here because we don't know what
    // anything actually looks like.
    
    // this is a constant
    public static final String[] VALID_GRADES = {"A", "B",...}
    // final means VALID_GRADES cannot change.
    
    
    public abstract double gpa();
    // this sets an expectation on the child classes. it must have 
    // this method.
    
}
```

For the children:

```java
public class LetterGrade extends Grade {

    private String grade; // Numberic Grade will be private int grade;
    

    @Override
    public double gpa() {
       // do something. convert letters grades to numbers or something.
    }

}
```

####Summary:

* The class and methods are `abstract`
* The abstract class cannot be called (cannot do `new AbstractClass`)

### Interface

* A class with no implementation.
* Defines 'services' 

```java

public interface IDed {
    public String getID();
}

public class Student extends Person implements IDed {
    // I am a Person that will follow IDed's rules.
    ...
}
```
But what if an ID is an `int` and not a `String`?

```java
public interface IDed<T> {
    public T getID(); 
}

public class Student extends Person implements IDed<String> {
    // I am a Person that will follow IDed's rules.
    ...
}
```

## Generic Arrays and Data Structures

### Generic Arrays

```java
package types;
public class GenericThingee<T> {
    private T[] contents;
    private T[][] square;
    
    public GenericThingee(int size) {
    this.contents = (T[]) new Object[size]; 
    this.square = (T[][]) new Object[size][size];
    }
}
```

### Data Structures

#### Queue

```java
public static void demoQueues() {
    // Construt queue
    Queue<String> lineUp = ConcurrentLinkedQueue<>();
    
    // Add things to queue
    lineUp.add("first in line");
    lineUp.add("second line");
    lineUp.add("third in line");
    
    // Remove one item
    lineUp.poll();
}
```

#### Map

```java
import java.util.Map
import java.util.HashMap

private Map<String, Grade> courseToGrade;

public Student(...) {
    ...
    //                       HashMap<> works fine
    this.courseToGrade = new HashMap<String, Grade>();
}

public void addGrade(String course, Grade g){
    this.courseToGrade.put(course, g);
}
```

Notice how `ConcurrentLinkedQueue` tells us how it is implemented under the hood. This is so we can decide what type of data structure to use.


## Recap

[Java Tutor](http://pythontutor.com)

### Garbage Collection

An used objected is called an unrefereced object, because it is no longer referenced by any port of the program.

Java does automatic garbage collecion.

### APIs

* Application Programming Interface
  * The public face of a piece of software 

###Equality

* Two variables that reference the same object
* Two objects that have the same content

* `==` checks if they are the same object.
* `.equals` checks if they contain the same values.

The meaning of `==` in java is not the same as the one in Python.


### New

```java
String s1 = "Happy";
String s2 = "Ha" + "ppy";
```
Here `s2` and `s1` are equivalent and they *reference the same object*, so `s1 == s2`

### Comparisons

If a `compareTo`  is not defined in a class, then it cannot be compared since class `Object` does not have a `compareTo` method.

```java
public abstract calss Grade implements Comparable<Grade> {
    ...
    
    @Override
    public int compareTo(Grade other){
        return (new Double(this.gpa()).compareTo(new Double(other.gpa())));
    }
}

```

## CRC Cards

* __C__lass __R__esponsibility __C__ollaboration cards
  *  Class: object-oriented class name, w/ super-sub classes
  *  Responsibility: what info is stored
  *  Collaboration: relationship w/ other classes
* a tool/method for system analysis and design
* interactiv/human-intensive
* Final Result: definition of classes and their relationship.
* *What* rather than *how*.


<table style="width:100%">
   <td colspan="2">Class Name</td>
  <tr>
    <td>Class Responsibility</td>
    <td>Class Relationship</td> 
  </tr>
</table>


## Exceptions

* Report unusual things, halting methods down the stack until
    * one fo the methods deals with the problem
    * the entire stack is empty

#### Exceptions in Java 

* To "throw an exception":
    * `throw Throwable;`
* To "catch an excpetoin":

    ```java
    try {
      statements
    } catch (Throwable parameter) {
        statements
    }
    ```

## Persistent Data

Making data available on fresh runs of an app.

What does the code look like?

* If previous data exists
    * load it
* else
    * Initialize it   
* Do stuff
* Save the state of the file.

Instead of writin the instance variables in the file, 
we can *serialize* the data.

Java has a `Serializable` class.


## Logs

Logs are a record of events, useful for history and diagnosing problems.

#### Logging
* The *process* of recording events that occur during the *execution* of a program.

#### stdin, stdout, stderr
* Streams between a program and its environment
  * stdin: standard input system (def: keyboard)
  * stdout: standard output system (def: terminal)
  * stderr: standard error stream (def: terminal)
The defaults can be overriden.

```
$ command > myfiles.txt < inputfile.txt
``` 
The commands runs, and input comes from `inputfile.txt`, output goes to `myfiles.txt`, the order does not matter.

```
$ cat movies.txt | grep to
```
Gives back movies with "to" in them

```
$ cat movies.txt | grep to | wc
```
lists movies with "to" and the word count.

### Logging in Java

#### java.util.logging.Logger

This class provides logging.




* SEVERE (highest severity)
* WARNING
* INFO
* CONFIG
* FINE
* FINER
* FINEST (lowest severity)

By defualt, only `INFO` or higher is shown.

The log messages get given to a handler.

## Design Patterns

A *design pattern* is a general description of a solution to a 
well-established problem and the "shape" of the code that solves it.
They are not specific to any one programming language, and are a mean of
communicating design ideas.

### Observer Design Pattern

* Problem: Need to main consistency between related objects, where one is dependent on the other, and should notify other objects w/o making assumptions on what those objects are

### Iterator Design Patter

Say you have the following:
```
for item in lst:
```

The variable `item` iterates over the items in `lst`.

There's something under the hood that allows iteration.

In java, some elements belong to an iterable class.

```java
/**
* AddressBook manages Contact, which has
* a name (first and last), email, number, etc
*/
 
class AddressBook implements Iterable<Contact>{
    private List<Contact> contacts;
    
    // required
    public Iterator<Contact> iterator() {
        return new AddressBookIterator();
    }
    
    // notice this is a class in a class
    private class AddressBookIterator implements Iterator<Contact> {
        private int current = 0;
        
        public boolean hasNext() {
            return current < contacts.size;
        }
        
        public Contact next() {
            
            // also needs to catch exceptions, etc
            return contacts.get(current);
            
            current += 1
        }
    }
}


``` 

Iterators (in java) have to provide a next method and a hasnext method.


## Regular Expressions

A *regular expression* is a pattern that a string matches or doesn't.

<table style="width:100%">
   <td colspan="3">Regex Expressions</td>
  <tr>
    <td>Pattern</td>
    <td>Matches</td>
    <td>Explanation</td> 
  </tr>
  <tr>
    <td>a*</td>
    <td>'', 'a', 'aa'</td>
    <td>Zero or more</td> 
  </tr>
  <tr>
    <td>b+</td>
    <td>'b', 'bb'</td>
    <td>One or more</td> 
  </tr>
  <tr>
    <td>ab?c</td>
    <td>'ac', 'abc'</td>
    <td>Zero or one</td> 
  </tr>
  <tr>
    <td>[abc]</td>
    <td>'a', 'b', 'c'</td>
    <td>One in the set</td> 
  </tr>
</table>

## Floating Point

 Add similar numbers together.




<style>
*{
  background-color: #5f5f5 !important;
  color: #333;
}
.push{
margin-left: 119px;
}
.pushl{
margin-left: 80px;
}

/* pre:hover, code:hover{
  font-size: 18px;
} */

a:hover{
color: skyblue;
}
li a {
padding-left: 5px;
}
li a:hover{
    border-left: 4px solid steelblue;
    transition: all 0.2s;
}

h2:hover {
border-width: 4px;
border-color: lightsteelblue;
}


</style>
