#include <stdio.h>

int main() {

int int1[5];
int int2[5];
int sum;
int i;
for (i = 0; i < 5; i++) {
    scanf("%d  %d", &int1[i], &int2[i]);
} 

for (i = 0; i < 5; i++) {
	sum = int1[i] + int2[i];
 printf("%d plus %d equals %d \n", int1[i], int2[i], sum);
}

int int3[4] = {5,7,18,20};

for (i = 0; i < 4; i++) {
  int3[i]++;
	printf("the element in %d now has the value %d \n", i, int3[i] );
}
// int3[4] = 120;
return 0;
}
