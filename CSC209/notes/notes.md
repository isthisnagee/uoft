




<h1>CSC209 - Software Tools and System Programming</h1>

<h2>Table of Contents</h2>
[TOC]

## Introduction

Class [website](http://www.cdf.toronto.edu/~csc209h/winter)

Prep [PCRS](https://teach.cdf.toronto.edu/209/content/quests)

Instructor: [David Liu](mailto:david@cs.toronto.edu), BA4260

* We are going to discuss software tools in the Unix tradition, the ones that come preinstalled w/ a traditional unix environment, and then writing them.
* We will do shell programming and writing software tools with C.
* The main part of this course is writing programs that interact w/ the OS.


## Unix Shell

* **Process**: executing instance of a program
* **Process control block**: OS data structure storing information about the
    process.

When you type a command in the shell, you tell it to start a new process.
The process control block stores process state, number, a program counter, registers, memory, etc.

```
ps
```

Shows the running processes. `ps` will show a PID, to kill a process, run `sudo kill PID`.

* **File**: a sequence of bytes
* **Inode**: data structure containing information about a file
* **Directory**: special kind of file mapping filenames to inodes.

Directories are themselves considered files(!).

###Permissions

When you run `ls -l`, strings like `dr-xr-xr-x` show up. These are file
permissions. These are read as:

```
-  ---   ---   ---
   [u]sr [g]rp [o]thr
```

* `r` read
* `w` write
* `x` execute
* The first character stands for the type
  * `-` regular file
  * `-` symbolic link
  * `d` directory

Directories also have permissions.

```
chmod
```

Is the command used to change permissions.

Say we have a file `hello` with permissions `-rwx------`. Running
`chmod u-x hello` removes the executable privelage. `u-x` means remove (`-`) the execute (`x`) privelage. To add 
it back, `chmod u+x hello`.