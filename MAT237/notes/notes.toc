\select@language {english}
\contentsline {section}{\numberline {1}Uniform Continuity}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Big Theorem}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Derivatives}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Derivative in $n$ dimensions}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Multivariable $\mathbb R^n$ to $\mathbb R$}{4}{subsection.2.2}
\contentsline {subsubsection}{Partial Derivatives}{6}{section*.2}
\contentsline {subsubsection}{Directional Derivatives}{8}{section*.3}
\contentsline {subsection}{\numberline {2.3}Derivatives of functions from $\mathbb R^n \rightarrow \mathbb R^m$}{8}{subsection.2.3}
\contentsline {subsubsection}{Chain Rule}{10}{section*.4}
\contentsline {subsubsection}{Intuition for the Derivative}{10}{section*.5}
\contentsline {subsection}{\numberline {2.4}Mean Value Theorem}{10}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Higher Order Derivatives}{11}{subsection.2.5}
\contentsline {subsubsection}{3rd and Higher Order Partials}{12}{section*.6}
\contentsline {subsubsection}{Multi-index Notation}{12}{section*.7}
\contentsline {subsection}{\numberline {2.6}Taylor Series}{13}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Optimization}{14}{subsection.2.7}
\contentsline {subsubsection}{Lagrange Multipliers}{15}{section*.8}
\contentsline {subsection}{\numberline {2.8}Multivariate Inverse Theorem}{17}{subsection.2.8}
